package com.domain.model.response

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ArtistSearchResponse(
    val resultCount: Int?=0,
    var results: List<Artist>
)

@JsonClass(generateAdapter = true)
data class Artist(
    val artistName: String? = "",
    val trackName: String? = "",
    val artworkUrl100: String? = "",
    val previewUrl: String? = "",
    val collectionName: String? = "",
    var isPlaying : Boolean= false
)
