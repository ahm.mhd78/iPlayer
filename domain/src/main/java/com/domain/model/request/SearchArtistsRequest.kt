package com.domain.model.request

data class SearchArtistsRequest(var searchKey: String = "") {
    fun map() = HashMap<String, String>().apply {
        put("term", searchKey)
        put("limit", "25")
    }
}
