package com.domain.repository

import com.domain.model.request.*
import com.domain.model.response.ArtistSearchResponse
import kotlinx.coroutines.flow.Flow


interface SongsRepository {
    suspend fun searchArtist(params: SearchArtistsRequest): Flow<ArtistSearchResponse?>
}