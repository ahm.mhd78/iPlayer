package com.domain.usecase.search

import com.domain.model.request.SearchArtistsRequest
import com.domain.model.response.ArtistSearchResponse
import com.domain.repository.SongsRepository
import com.domain.usecase.base.UseCase
import javax.inject.Inject

class SearchArtistsUseCase @Inject constructor(var songsRepository: SongsRepository) :
    UseCase<ArtistSearchResponse?, SearchArtistsRequest>() {

    override suspend fun run(params: SearchArtistsRequest) =
        songsRepository.searchArtist(params)


}