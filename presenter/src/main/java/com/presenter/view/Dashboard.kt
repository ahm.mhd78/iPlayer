package com.presenter.view

import androidx.activity.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.base.ui.BaseActivity
import com.presenter.R
import com.presenter.databinding.ActivityDashboardBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class Dashboard : BaseActivity(R.layout.activity_dashboard) {


    private val viewModel: DashboardViewModel by viewModels()
    lateinit var viewBinding: ActivityDashboardBinding
    lateinit var adapter: SongsAdapter

    override fun initializeComponents() {
        with(binding as ActivityDashboardBinding) {
            viewModel = this@Dashboard.viewModel
            viewBinding = this
            lifecycleOwner = this@Dashboard
        }

        initAdapter(viewBinding.list)
        viewModel.searchArtist()
    }

    override fun setObservers() {
        observeDataEvents(viewModel)

        viewModel.obEvents.observe(this) {
            when (val event = it.getEventIfNotHandled()) {
                is DashboardEvents.OnListRefreshed -> {
                    adapter.clearList()
                    adapter.addItems(event.list)
                }
            }
        }

        viewModel.isPlaying.observe(this){
            val item = viewModel.playingItem.value
            item?.isPlaying = it
            adapter.notifyDataSetChanged()
        }

    }


    private fun initAdapter(rv: RecyclerView) {
        adapter = SongsAdapter {
            viewModel.play(it)
            adapter.notifyDataSetChanged()
        }
        rv.adapter = adapter
    }


    override fun onDestroy() {
        viewModel.release()
        super.onDestroy()
    }

}