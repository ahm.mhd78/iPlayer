package com.presenter.view

import com.base.adapters.BaseAdapter
import com.domain.model.response.Artist
import com.presenter.R
import com.presenter.databinding.LiSongBinding

class SongsAdapter(val onTap: (Artist) -> Unit) :
    BaseAdapter<Artist, LiSongBinding>(R.layout.li_song) {

    override fun onItemInflated(items: Artist, position: Int, binding: LiSongBinding) {
        binding.model = items
    }

    override fun onClick(item: Artist) {
        getList().forEach {
            it.isPlaying = false
        }
        onTap(item)
    }
}