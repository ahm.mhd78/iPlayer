package com.presenter.view

import com.domain.model.response.Artist

sealed class DashboardEvents {
    class OnListRefreshed(val list: List<Artist>) : DashboardEvents()
}
