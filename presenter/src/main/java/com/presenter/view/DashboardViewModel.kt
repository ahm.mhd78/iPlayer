package com.presenter.view

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.base.events.BaseEvent
import com.base.extensions.default
import com.base.viewmodels.BaseViewModel
import com.domain.model.request.SearchArtistsRequest
import com.domain.model.response.Artist
import com.domain.usecase.search.SearchArtistsUseCase
import com.presenter.util.MusicPlayer
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class DashboardViewModel @Inject constructor(
    private val searchUsecase: SearchArtistsUseCase,
    private val player: MusicPlayer
) :
    BaseViewModel<DashboardEvents>() {

    val playingItem = MutableLiveData<Artist>()
    val isPlaying = MutableLiveData<Boolean>().default(false)


    val request = SearchArtistsRequest()
    private var job: Job? = null
    val noRecords = MutableLiveData<Int>().default(View.GONE)

    fun searchArtist() {
        if (job?.isActive == true)
            job?.cancel()

        job = viewModelScope.launch {
            showLoader()
            searchUsecase(request).catch { exp ->
                handleExceptions(exp)
                hideLoader()
            }.collect {
                hideLoader()
                onDataLoaded(it?.results ?: emptyList())

                if (it?.results.isNullOrEmpty())
                    noRecords.value = View.VISIBLE
                else
                    noRecords.value = View.GONE
            }
        }
    }

    fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        request.searchKey = s.toString()
        searchArtist()
    }

    private fun onDataLoaded(list: List<Artist>) {
        _events.value = BaseEvent(DashboardEvents.OnListRefreshed(list))
    }

    fun play(it: Artist) {
        viewModelScope.launch(Dispatchers.Default) {
            withContext(Dispatchers.Main) {
                isPlaying.value = true
                playingItem.value = it
            }
            player.play(it)
        }
    }

    fun togglePlayPause() {
        player.togglePausePlay {
            isPlaying.value = it
            playingItem.value?.isPlaying = it
        }
    }

    fun release() {
        isPlaying.value = false
        player.release()
    }
}