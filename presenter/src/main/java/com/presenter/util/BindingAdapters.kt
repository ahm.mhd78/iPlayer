package com.presenter.util

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.presenter.R

@BindingAdapter("bind:load")
fun ImageView.load(url: String?) {
    Glide.with(context)
        .load(url)
        .placeholder(R.drawable.album_placeholder)
        .error(R.drawable.album_placeholder)
        .into(this)
}

@BindingAdapter("bind:gif")
fun ImageView.loadGif(drawable: Drawable) {
    Glide.with(context)
        .load(drawable)
        .into(this)
}