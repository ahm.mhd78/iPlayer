package com.presenter.util

import android.media.MediaPlayer
import com.domain.model.response.Artist
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class MusicPlayer @Inject constructor() {

    var item: Artist? = null
    var player: MediaPlayer? = null

    fun play(item: Artist) {
        stop()

        this.item = item
        player = MediaPlayer()
        try {
            this.item?.isPlaying = true
            player?.setDataSource(item.previewUrl)
            player?.prepare()
            player?.start()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun stop() {
        item?.isPlaying = false
        player?.stop()
    }

    fun togglePausePlay(callback: (Boolean) -> Unit) {
        if (player?.isPlaying == true) {
            player?.pause()
            callback(false)
        } else {
            player?.start()
            callback(true)
        }
    }

    fun release() {
        stop()
        player?.release()
    }

}