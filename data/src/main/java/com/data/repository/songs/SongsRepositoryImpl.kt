package com.data.repository.songs

import com.domain.model.request.SearchArtistsRequest
import com.domain.model.response.ArtistSearchResponse
import com.domain.repository.SongsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SongsRepositoryImpl @Inject constructor(private val remote: SongsRemoteSource) : SongsRepository {
    override suspend fun searchArtist(params: SearchArtistsRequest): Flow<ArtistSearchResponse?>  =
        remote.searchArtist(params)

}