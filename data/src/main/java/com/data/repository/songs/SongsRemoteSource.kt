package com.data.repository.songs

import com.data.network.API
import com.data.network.NetworkCall
import com.domain.model.request.SearchArtistsRequest
import com.domain.model.response.ArtistSearchResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SongsRemoteSource @Inject constructor(private val networkCall: NetworkCall) {

    suspend fun searchArtist(params: SearchArtistsRequest): Flow<ArtistSearchResponse?> =
        networkCall.get(
            API.SEARCH, params.map()
        )

}