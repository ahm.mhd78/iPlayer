package com.data.datastore

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.data.fromJson
import com.data.toJson
import com.squareup.moshi.Moshi
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import org.json.JSONException
import javax.inject.Inject
import javax.inject.Singleton

val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "DATASTORE")

@Singleton
class DataStoreManager
@Inject constructor(
    val moshi: Moshi,
    @ApplicationContext val context: Context
) {
    suspend fun putToken(value: String) = context.dataStore.edit {
        it[PreferenceKeys.AUTH_TOKEN] = "Bearer $value"
    }

    fun getToken() = context.dataStore.data.map {
        it[PreferenceKeys.AUTH_TOKEN]
    }.flowOn(Dispatchers.IO)

    fun getRefreshToken() = context.dataStore.data.map {
        it[PreferenceKeys.REFRESH_TOKEN]
    }.flowOn(Dispatchers.IO)

    suspend fun putRefreshToken(refreshToken: String) = context.dataStore.edit {
        it[PreferenceKeys.REFRESH_TOKEN] = refreshToken
    }

    @Throws(JSONException::class)
    suspend inline fun <reified T>putObject(key: Preferences.Key<String>, obj: T) {
        var json = moshi.toJson(obj) ?: ""
        context.dataStore.edit {
            it[stringPreferencesKey("$key")] = json
        }
    }

    inline fun <reified T> getObject(key: Preferences.Key<String>) = context.dataStore.data.map {
        it[stringPreferencesKey("$key")]
    }.map {
        moshi.fromJson<T>(it ?: "")
    }.flowOn(Dispatchers.IO)

    suspend fun clear() {
        context.dataStore.edit {
            it.clear()
        }
    }

}



