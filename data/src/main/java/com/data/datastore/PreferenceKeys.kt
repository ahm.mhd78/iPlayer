package com.data.datastore

import androidx.datastore.preferences.core.stringPreferencesKey

object PreferenceKeys {
    val AUTH_TOKEN = stringPreferencesKey("AUTH_TOKEN")
    val REFRESH_TOKEN = stringPreferencesKey("REFRESH_TOKEN")
    val PROFILE = stringPreferencesKey("PROFILE")
}