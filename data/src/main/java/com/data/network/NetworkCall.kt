package com.data.network

import TypeReference
import com.data.fromJson
import com.data.util.Applog
import com.domain.exceptions.ServerException
import com.domain.exceptions.UnProcessableEntityException
import com.domain.exceptions._401Exception
import com.domain.exceptions._404Exception
import com.squareup.moshi.Moshi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Dell 5521 on 9/28/2019.
 */
@Singleton
class NetworkCall @Inject constructor(
    var apiInterface: ApiInterface,
    var moshi: Moshi
) {
    //creating general methods for calling GET/POST request
    @Throws(Exception::class)
    suspend inline fun <reified T> generalRequest(
        crossinline request: suspend () -> Response<ResponseBody>
    ): Flow<T?> =
        flow {
            val response = request()
            if (response.isSuccessful) {
                var responseString = response.body()?.string()
                val type = object : TypeReference<T>() {}.type
                val model = moshi.fromJson<T>(responseString ?: "", type)
                Applog.d(responseString.toString())
                emit(model)
            } else if (response.code() == 422) {
                var errorResponse = response.errorBody()?.string() ?: "{}"
                Applog.d(errorResponse.toString())
                throw UnProcessableEntityException(handle422Error(errorResponse))
            } else if (response.code() == 500) {
                var errorResponse = response.errorBody()?.string() ?: "{}"
                Applog.d(errorResponse.toString())
                throw ServerException()
            } else if (response.code() == 404) {
                Applog.d("404")
                throw _404Exception()
            } else if (response.code() == 401) {
                var errorResponse = response.errorBody()?.string() ?: "{}"
                Applog.d(errorResponse.toString())
                throw _401Exception(handleErrors(errorResponse))
            } else {
                var errorResponse = response.errorBody()?.string() ?: "{}"
                Applog.d(errorResponse.toString())
                throw Exception(handleErrors(errorResponse))
            }
        }


    //simple POST with data map
    @Throws(Exception::class)
    suspend inline fun <reified T> post(
        endpoint: String,
        bodyMap: Map<String, String>?
    )//data params
            : Flow<T?> {

        val requestBodyMap = HashMap<String, @kotlin.jvm.JvmSuppressWildcards RequestBody>()
        if (bodyMap != null) {
            for ((key, value) in bodyMap) {
                requestBodyMap[key] = RequestBody.create(MediaType.parse("text/plain"), value)
            }
        }

        return if (bodyMap == null) {
            generalRequest<T>({ apiInterface.post(endpoint) })
        } else {
            generalRequest<T>({ apiInterface.post(endpoint, requestBodyMap) })
        }
    }

    //simple POST with data map
    @Throws(Exception::class)
    suspend inline fun <reified T> patch(
        endpoint: String,
        bodyMap: Map<String, String>?
    )//data params
            : Flow<T?> {

        val requestBodyMap = HashMap<String, @kotlin.jvm.JvmSuppressWildcards RequestBody>()
        if (bodyMap != null) {
            for ((key, value) in bodyMap) {
                requestBodyMap[key] = RequestBody.create(MediaType.parse("text/plain"), value)
            }
        }

        return generalRequest<T> { apiInterface.patch(endpoint, requestBodyMap) }
    }

    //simple POST with data map
    @Throws(Exception::class)
    suspend inline fun <reified T> post(
        endpoint: String,
        bodyMap: String
    )//data params
            : Any? {
        val requestBody = RequestBody.create(MediaType.parse("application/json"), bodyMap)


        return generalRequest<T>(suspend { apiInterface.post(endpoint, requestBody) })
    }


    //post method with query map
    @Throws(Exception::class)
    suspend inline fun <reified T> post(
        endpoint: String,
        bodyMap: String,
        queryMap: Map<String, String>
    )//data params
            : Any? {

        val requestBody = RequestBody.create(MediaType.parse("application/json"), bodyMap)


        return generalRequest<T>({ apiInterface.post(endpoint, requestBody, queryMap) })
    }

    @Throws(Exception::class)
    suspend inline fun <reified T> get(
        endpoint: String,
        queryMap: Map<String, String>? //query params
    ): Flow<T?> {

        return if (queryMap == null)
            generalRequest<T> { apiInterface.get(endpoint) }
        else
            generalRequest<T> { apiInterface.get(endpoint, queryMap) }
    }

    @Throws(Exception::class)
    suspend inline fun <reified T> delete(
        endpoint: String
    ): Any? {
        return generalRequest<T> { apiInterface.delete(endpoint) }
    }


    fun handle422Error(
        errorResponse: String
    ): HashMap<String, String> {
        val errorMap = HashMap<String, String>()

        var err: JSONObject = JSONObject(errorResponse)
        if (err.has("errors")) {
            err = err.getJSONObject("errors")
        }
        if (err.has("error")) {
            err = err.getJSONObject("error")
        }

        val keys = err.keys()
        while (keys.hasNext()) {
            var dynamicKey = keys.next().toString()
            try {
                val jsonArray = err.getJSONArray(dynamicKey)
                if (jsonArray.length() > 0) {
                    errorMap.put(dynamicKey, jsonArray.getString(0))
                }
            } catch (e: Exception) {

            }
        }
        return errorMap
    }

    fun handleErrors(
        errorResponse: String
    ): String {
        var err: JSONObject = JSONObject(errorResponse)
        if (err.has("message"))
            return err.get("message").toString()

        if (err.has("error"))
            return err.get("error").toString()

        if (err.has("msg"))
            return err.get("msg").toString()

        return "Something went wrong"
    }
}
