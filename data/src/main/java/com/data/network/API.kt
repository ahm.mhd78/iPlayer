package com.data.network

import com.data.BuildConfig

object API {

    private const val API = BuildConfig.BASE_URL
    const val SEARCH = "${API}search"
}
