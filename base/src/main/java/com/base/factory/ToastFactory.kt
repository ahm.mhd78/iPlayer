package com.base.factory

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.DrawableRes
import com.base.R
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject
import javax.inject.Singleton

@ActivityScoped
class ToastFactory @Inject constructor(@ApplicationContext private var context: Context) {

    private lateinit var toast: Toast
    private lateinit var view: View

    fun create(message: String,@DrawableRes bg : Int? = R.drawable.bg_toast) {

        if (::toast.isInitialized && toast.view?.isShown!!) toast.cancel()

        toast = Toast(context)

        if (!::view.isInitialized)
            view = LayoutInflater.from(context).inflate(R.layout.toast_layout, null)

        view.setBackgroundResource(bg ?: R.drawable.bg_toast)

        toast.view = view
        toast.duration = Toast.LENGTH_SHORT
        toast.view?.findViewById<TextView>(R.id.message)?.text = message
        toast.show()

    }
}