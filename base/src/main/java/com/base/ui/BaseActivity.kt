package com.base.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.base.R
import com.base.databinding.MessageDialogBinding
import com.base.events.BaseDataEvents
import com.base.extensions.gone
import com.base.extensions.visible
import com.base.factory.ToastFactory
import com.base.viewmodels.BaseViewModel
import javax.inject.Inject

abstract class BaseActivity(@LayoutRes private val layoutId: Int) : AppCompatActivity() {

    val TAG: String = this.javaClass.simpleName

    @Inject
    lateinit var toastFactory: ToastFactory

    lateinit var binding: ViewDataBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layoutId)
        initializeComponents()
        setObservers()
        setUpListeners()
    }

    open fun setUpListeners() {}
    protected abstract fun initializeComponents()
    protected abstract fun setObservers()
    open fun loaderView(): View = findViewById(R.id.loaderView)


    fun observeDataEvents(viewModel: BaseViewModel<*>) {
        viewModel.obDataEvent.observe(this, Observer {
            var event = it.getEventIfNotHandled()
            if (event != null)
                when (event) {
                    BaseDataEvents.ShowLoader -> showLoader()
                    BaseDataEvents.HideLoader -> hideLoader()
                    is BaseDataEvents.Exception -> showException(event.message)
                    is BaseDataEvents.Error -> showError(event.message)
                    is BaseDataEvents.Toast -> showToast(event.message)
                }
        })
    }


    protected fun showToast(message: String) {
        toastFactory.create(message)
    }

    protected open fun showException(message: String) {
        showToast(message)
    }

    protected open fun showError(message: String) {
        showToast(message)
    }

    protected fun showLoader() {
        loaderView().visible()
    }

    protected fun hideLoader() {
        loaderView().gone()
    }


}