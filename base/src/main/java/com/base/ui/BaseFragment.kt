package com.base.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.annotation.DrawableRes
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.base.R
import com.base.events.BaseDataEvents
import com.base.extensions.gone
import com.base.extensions.visible
import com.base.factory.ToastFactory
import com.base.util.KeyboardUtil
import com.base.viewmodels.BaseViewModel
import javax.inject.Inject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
abstract class BaseFragment(@LayoutRes private val layoutID: Int) : Fragment() {

    val TAG = this::class.java.name

    @Inject
    lateinit var keyboardUtil: KeyboardUtil

    @Inject
    lateinit var toastFactory: ToastFactory

    lateinit var binding: ViewDataBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(LayoutInflater.from(requireContext()), layoutID, null, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activateHideKeyboardUponTouchingScreen(view)

        initializeComponents()
        setUpListeners()
        setObservers()

    }

    fun screenTransactions() = (activity as BaseActivity)

    open fun setUpListeners() {}
    abstract fun initializeComponents()
    abstract fun setObservers()

    override fun onResume() {
        super.onResume()
        hideKeyboard()
    }

    override fun onPause() {
        super.onPause()
        hideKeyboard()
    }

    private fun activateHideKeyboardUponTouchingScreen(view: View) {

        if (view !is EditText) {
            view.setOnTouchListener { _, _ ->
                keyboardUtil.closeKeyboard(requireActivity())
                return@setOnTouchListener false
            }
        }
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val v = view.getChildAt(i)
                activateHideKeyboardUponTouchingScreen(v)
            }
        }
    }

    fun observeDataEvents(viewModel: BaseViewModel<*>) {
        viewModel.obDataEvent.observe(this, Observer {
            var event = it.getEventIfNotHandled()
            if (event != null)
                when (event) {
                    BaseDataEvents.ShowLoader -> showLoader()

                    BaseDataEvents.HideLoader ->
                        hideLoader()
                    is BaseDataEvents.Exception ->
                        showException(event.message, null)

                    is BaseDataEvents.Error ->
                        showError(event.message, null)

                    is BaseDataEvents.Toast -> {
                        showToast(event.message, event.bg)
                    }
                    else -> {
                    }
                }
        })

    }

    fun onProgressCanceled() {}

    protected fun hideKeyboard() {
        keyboardUtil.closeKeyboard(requireActivity())
    }

    protected fun showToast(message: String, @DrawableRes bg: Int?) {
        toastFactory.create(message, bg)
    }

    protected fun showException(message: String, @DrawableRes bg: Int?) {
        showToast(message, bg)
    }

    protected fun showError(message: String, @DrawableRes bg: Int?) {
        showToast(message, bg)
    }


    open fun loaderView(): View? = binding.root.findViewById(R.id.loaderView)

    open fun showLoader() {
        loaderView()?.visible()
    }

    open fun hideLoader() {
        loaderView()?.gone()
    }

}
