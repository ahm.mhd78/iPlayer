package com.base.viewmodels

import androidx.annotation.DrawableRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.base.events.BaseDataEvents
import com.base.events.BaseEvent
import com.domain.exceptions.*

open class BaseViewModel<T> : ViewModel() {
    val TAG = this::class.java.name

    protected val _events: MutableLiveData<BaseEvent<T>> = MutableLiveData()
    val obEvents: LiveData<BaseEvent<T>> = _events

    protected val _dataEvent: MutableLiveData<BaseEvent<BaseDataEvents>> = MutableLiveData()
    val obDataEvent: LiveData<BaseEvent<BaseDataEvents>> = _dataEvent

    suspend fun handleExceptions(exception: Throwable): HashMap<String, String> {
        val errors: HashMap<String, String> = HashMap()
        when (exception) {
            is ConnectivityException, is _404Exception, is ServerException, is _401Exception -> {
                showError(exception.message ?: "")
                errors.put("error", exception.message ?: "")
            }
            is UnProcessableEntityException -> {
                errors.putAll(exception.errorMap)
                exception.errorMap.forEach {
                    showError(it.value)
                }
            }
            else -> {
                showError(exception.message ?: "")
                errors.put("error", exception.message ?: "")
            }
        }
        return errors
    }

    protected fun showError(message: String) {
        _dataEvent.value = (BaseEvent(BaseDataEvents.Error(message)))
    }

    protected fun showException(exception: Exception) {

    }

    protected fun showLoader() {
        _dataEvent.value = BaseEvent(BaseDataEvents.ShowLoader)
    }

    protected fun hideLoader() {
        _dataEvent.value = BaseEvent(BaseDataEvents.HideLoader)
    }


    protected fun showToast(message: String, @DrawableRes bg: Int? = null) {
        _dataEvent.value = BaseEvent(BaseDataEvents.Toast(message, bg))
    }

}