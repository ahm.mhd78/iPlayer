package com.base.events

import androidx.annotation.DrawableRes

sealed class BaseDataEvents {
    object ShowLoader : BaseDataEvents()
    object HideLoader : BaseDataEvents()
    class Exception(val message: String) : BaseDataEvents()
    class Error(val message: String) : BaseDataEvents()
    class Toast(val message: String, @DrawableRes var bg: Int?) : BaseDataEvents()
}