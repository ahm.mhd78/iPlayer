package com.base.extensions

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.MutableLiveData
import com.base.ui.BaseActivity

/*Live data extension function which'll set initial value*/
fun <T : Any?> MutableLiveData<T>.default(initialValue: T) = apply { setValue(initialValue) }

fun BaseActivity.addFragment(
    containerId: Int,
    fragment: Fragment,
    allowMultipleInstances: Boolean = false
) {
    supportFragmentManager.transact {
        add(containerId, fragment)
    }
}

fun BaseActivity.popStack() {
    supportFragmentManager.popBackStack()
}

fun BaseActivity.clearStack() {
    for (i in 0 until supportFragmentManager.backStackEntryCount) {
        supportFragmentManager.popBackStack()
    }
}

/**
 * Runs a FragmentTransaction, then calls commit().
 */
private inline fun FragmentManager.transact(action: FragmentTransaction.() -> Unit) {
    beginTransaction().apply {
        action()
    }.commitAllowingStateLoss()

}

